import Vue from 'vue'
import Vuex from 'vuex'
// 引入持久化插件
import createPersistedState  from "vuex-persistedstate"
// 
import leftMenu from './modules/leftMenu'
import user from "./modules/user"
// 
Vue.use(Vuex)

const store = new Vuex.Store({
    modules:{
        leftMenu,user
    },
    state:{
    },
    mutations:{
        change(state,num){
            return state+=num
        }
    },
    // vuex持久化插件
    plugins: [
        createPersistedState({
            storage: window.sessionStorage
        })
    ],

})

export default store