

export default {
    namespaced: true,
    state:{
        token:null,
        user:null,
        menu:null
    },
    mutations:{
        setToken(state,data){
            state.token = data
        },
        setUser(state,data){
            state.user = data
        },
        setMenu(state,data){
            state.menu = data
        }
    },
    getters :{
        getToken(state){
            return state.token
        },
        getUser(state){
            return state.user
        }
    }
}