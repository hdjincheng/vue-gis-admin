import Vue from 'vue'
import Router from 'vue-router'
import store from "../store/index"
Vue.use(Router)
// 项目整体框架
import Layout from "@/views/dasboard/index"
// 项目左侧菜单框架
import LeftLayout from "@/views/leftMenuDashboard/index"
import leftMenu from '../store/modules/leftMenu'
// const router = new Router({
//     routes:
// })
// 静态路由信息
function getStaticRoutes() {
    return  [              //配置路由，这里是个数组
        {
            path: '/',
            redirect: '/login',
            hidden: true
        },
        {
            path: '/login',
            component: () => import("@/views/mianviews/login/index"),
            hidden: true
        },
        {
            path: '/dasboard',
            meta: {
                title: '首页',
                icon: 'nested'
            },
            component: Layout,
            // hidden: false
            children: [
                {
                    path: '/',
                    meta: { title: '首页', icon: 'link' },
                    component: () => import('@/views/mianviews/map/index'),
                    hidden: true
                },

            ]
        },
        {
            path: '/system_ae',
            meta: { title: '用户管理', icon: 'link' },
            component: Layout,
            hidden: true,
            children: [
                {
                    path: '/system/user_ae',
                    name: "user_ae",
                    meta: { title: '人员信息', icon: 'link' },
                    component: () => import('@/views/mianviews/system/user/user'),
                },
                {
                    path: '/system/role_ae',
                    name: "role_ae",
                    meta: { title: '角色管理', icon: 'link' },
                    component: () => import('@/views/mianviews/system/role/role'),
                },
                {
                    path: '/system/access_ae',
                    name: 'access_ae',
                    meta: { title: '权限管理', icon: 'link' },
                    component: () => import('@/views/mianviews/system/access/access'),
                },

            ]
        },
    ]
}
// 创建路由
const createRouter = () => new Router({
    // mode: 'history', // require service support
    mode: "history",
    // base:"automonitor",
    scrollBehavior: () => ({ y: 0 }),
    routes: getStaticRoutes()
})
const router = createRouter()


/**
 * 路由前守卫导航
 */
router.beforeEach((to, from, next) => {
    // 判断是否是登录页
    if (to.path == "/login") {
        next()
    } else {
        const { token } = store.state.user
        // 校验token是否为空，不为空
        if (token) {
            next()
        } else {
            next({ path: '/login' })
        }
    }
})
// 获取持久化的token信息和菜单信息
const { menu } = store.state.user
// 动态添加菜单，放到router页面，一进入页面就要进行跳转，便可解决页面刷新导致进入404页面
if (menu)
    addMenus(menu)

/**
 * 重置路由
 */
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
    router.options = newRouter.options
}

export default router

/**
 * 根据菜单数据生成菜单路由
 * @param {*} menus 
 */
export function addMenus(menus) {
    let newMenus = []
    if (menus) {
        menus.map((item, index) => {
            let template = {
                path: item.url ? item.url : "",
                isChildren: false,
                left:true,
                meta: { title: item.name ? item.name : "", icon: item.icon ? item.icon : 'link' },
                component: Layout,
                children: []
            }
            if (item.children && item.children.length > 0) {
                template.isChildren = true
                let subMenus = item.children
                subMenus.map((jtem, j) => {
                    // 上级指定跳转到下一级
                    template.redirect= subMenus[0].url
                    let viewPath = jtem.path
                    // 判断路径第一个字符串
                    if (viewPath&&viewPath.substr(0, 1) == "/")
                        viewPath = viewPath.substr(1, viewPath.length - 1)
                    let subTemplate = {
                        path: jtem.url ? jtem.url : "",
                        meta: { title: jtem.name ? jtem.name : "", icon: jtem.icon ? jtem.icon : 'link' },
                        component: LeftLayout,
                        children:[ ]
                    }
                    if(jtem.children && jtem.children.length > 0){
                        subTemplate.isChildren = true
                        let threeMenus = jtem.children
                        threeMenus.map((ztem, z) => {
                              // 上级指定跳转到下一级
                            subTemplate.redirect= threeMenus[0].url
                            let viewPath = ztem.path
                            // 判断路径第一个字符串
                            if (viewPath&&viewPath.substr(0, 1) == "/")
                                viewPath = viewPath.substr(1, viewPath.length - 1)
                            let threeTemplate = {
                                path: ztem.url ? ztem.url : "",
                                meta: { title: ztem.name ? ztem.name : "", icon: ztem.icon ? ztem.icon : 'link' },
                                component: () => import(`@/${viewPath}`).catch(error => {
                                    router.push({
                                        path: "/404"
                                    })
                                }),
                            }
                            subTemplate.children.push(threeTemplate)
                        })
                    }else{
                        subTemplate.children.push({
                            path:  jtem.url ? jtem.url : "",
                            meta:{ title: jtem.name ? jtem.name : "", icon: jtem.icon ? jtem.icon : 'link' },
                            component: () => import(`@/${viewPath}`).catch(error => {
                                router.push({
                                    path: "/404"
                                })
                            }),
                        })
                    }
                    template.children.push(subTemplate)
                })
            } else {
                let viewPath = item.path
                // 判断路径第一个字符串，如果含有则去除
                if (viewPath&&viewPath.substr(0, 1) == "/")
                    viewPath = viewPath.substr(1, viewPath.length - 1)
                let noSubTemplate = {
                    path: '/',
                    meta: { title: item.name ? item.name : "", icon: item.icon ? item.icon : 'link' },
                    component: LeftLayout,
                    children:[{
                        path: '/',
                        meta: { title: item.name ? item.name : "", icon: item.icon ? item.icon : 'link' },
                        component: () => import(`@/views/${viewPath}`).catch(error => {
                            router.push({
                                path: "/404"
                            })
                        }),
                    }]
                }
                template.children.push(noSubTemplate)
            }
            newMenus.push(template)
            // 菜单信息添加到路由中
            router.options.routes.push(template);
        })
        // 404页面放到路由的最后
        newMenus.push({ path: '*', hidden: true, redirect: '/404' })
        // 动态添加路由
        router.addRoutes(newMenus)
    }
}

