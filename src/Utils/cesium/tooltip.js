/**
 * 提示工具实现的类库
 */
export default class ToolTip {
    constructor(viewer) {
        this._frameDiv = null
        this._div = null
        this._title = null
        if (!viewer)
            return
        this.init(viewer.container)
    }
    /**
     * 初始化提示信息
     * @param {*} frameDiv 
     */
    init(frameDiv) {
        var _this = this;

        var div = document.createElement('DIV');
        div.className = "twipsy right";

        var arrow = document.createElement('DIV');
        arrow.className = "twipsy-arrow";
        div.appendChild(arrow);

        var title = document.createElement('DIV');
        title.className = "twipsy-inner";
        div.appendChild(title);

        frameDiv.appendChild(div);

        _this._div = div;
        _this._title = title;
        _this._frameDiv = frameDiv;
    }
    /**
     * 设置提示工具的显示与关闭
     * @param {*} visible 
     */
    setVisible(visible) {
        this._div.style.display = visible ? 'block' : 'none';
    }
    // 根据位置和提示信息显示提示信息
    showAt(position, message) {
        if (position && message) {
            this.setVisible(true);
            this._title.innerHTML = message;
            this._div.style.left = position.x +180+ "px";
            this._div.style.top = (position.y - this._div.clientHeight / 2+50) + "px";
        }
    }

}