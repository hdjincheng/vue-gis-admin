/**
 *说明：三维的绘制工具
 */
// import * as Cesium from "cesium";

// import * as Cesium from "cesium"

//导入提示工具
import toolTip from "./tooltip"

export default class Z3DDraw {
    constructor(viewer) {
        if (!viewer)
            throw new Error("3D viewer对象不能为空")
        this.viewer = viewer
        // 绘制点的坐标信息
        this.positions = []
        // 绘制对象
        this.handler = null
        // 鼠标提示信息
        this.tooltip = null
        // 实体信息
        this.entities = []
    }
    /**
     * 绘制函数
     * @param {*} option 
     * {
     *  type:1, 数字，1代表绘制点，2代表绘制线，3代表绘制面
     *  end:(参数1,参数2)=>{绘制结束后的监听事件，参数1表示绘制的实体，参数2表示的坐标信息
     *  }
     * }
     */
    Draw(option) {
        let scene = this.viewer.scene
        // 将坐标信息置空
        this.positions = []
        // 创建提示工具对象
        if (!this.tooltip)
            this.tooltip = new toolTip(this.viewer)
        // 创建鼠标事件
        if (!this.handler)
            this.handler = new Cesium.ScreenSpaceEventHandler(scene.canvas);
        // 鼠标移动事件
        this.handler.setInputAction((movement) => {
            // 设置鼠标移动的提示信息
            if (this.positions.length < 1 && this.positions.length >= 0) {
                this.tooltip.showAt(movement.endPosition, "<p>点击开始绘制</p>")
            } else {
                this.tooltip.showAt(movement.endPosition, "<p>点击继续绘制,右键结束绘制</p>")
            }
            // 绘制线和面需要动态绘制
            if (option.type == 2 || option.type == 3) {
                var newPosition = this.getPosition(movement.endPosition);
                if (newPosition && this.positions.length > 0) {
                    this.positions.pop();
                    this.positions.push(newPosition);
                }
            }
        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
        // 左键
        this.handler.setInputAction((movement) => {
            var position = this.getPosition(movement.position)
            switch (option.type) {
                case 1:
                    this.drawPoint(position)
                    break
                case 2:
                    this.drawLine(position)
                    break
                case 3:
                    this.drawPlygon(position)
                    break
            }
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
        // 右键
        this.handler.setInputAction((movement) => {
            this.endDraw(option)
        }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);

    }
    /**
     * 结束绘制
     */
    endDraw(option) {
        this.handler.destroy();
        this.tooltip.setVisible(false)
        this.positions.pop();//最后一个点无效
        if (option.end) option.end(this.entities, this.position)
        this.entities = []

    }
    /**
     * 获取坐标信息
     * @param {*} position 
     */
    getPosition(position) {
        if (!Cesium.defined(position)) {
            return;
        }
        let ray = this.viewer.camera.getPickRay(position);
        if (!Cesium.defined(ray)) {
            return;
        }
        let cartesian = this.viewer.scene.globe.pick(ray, this.viewer.scene);
        if (!Cesium.defined(cartesian)) {
            return;
        }
        return cartesian
    }
    /**
     * 根据位置信息绘制点
     * @param {*} position 
     */
    drawPoint(position) {
        this.creatPoint(position)
        this.positions.push(position)
    }
    /**
     * 绘制线
     * @param {*} position 
     */
    drawLine(position) {
        if (position) {
            if (this.positions.length == 0) {
                this.positions.push(position)
                // 动态绘制
                let dynamicPositions = new Cesium.CallbackProperty(() => {
                    return this.positions;
                }, false);

                let entity = this.viewer.entities.add({
                    polyline: {
                        positions: dynamicPositions,
                        clampToGround: true,
                        width: 3,
                        material:Cesium.Color.AQUA
                        //  new Cesium.PolylineGlowMaterialProperty({
                        //     glowPower: 0.25,
                        //     color: Cesium.Color.fromCssColorString('#00f').withAlpha(0.9)
                        // })
                    }
                });
                this.entities.push(entity)
            }
            this.positions.push(position)
            this.creatPoint(position)
        }
    }
    /**
     * 绘制多边形
     * @param {*} position 
     */
    drawPlygon(position) {
        if (position) {
            if (this.positions.length == 0) {
                this.positions.push(position)
                var dynamicPositions = new Cesium.CallbackProperty(() => {
                    if (this.positions.length > 2) {
                        var pHierarchy = new Cesium.PolygonHierarchy(this.positions);
                        return pHierarchy;
                    } else {
                        return null;
                    }
                }, false);
                if (dynamicPositions) {
                    let entity = this.viewer.entities.add({
                        polygon: {
                            hierarchy: dynamicPositions,
                            material: new Cesium.ColorMaterialProperty(Cesium.Color.WHITE.withAlpha(0.7))
                        }
                    });
                    this.entities.push(entity)
                }
            }
            this.positions.push(position)
            this.creatPoint(position)
        }
    }
    /**
     * 根据坐标创建点对象
     * @param {*} position 
     */
    creatPoint(position) {
        if (position) {
            let entity = this.viewer.entities.add({
                name: "点几何对象",
                position: position,
                point: {
                    color: Cesium.Color.RED,
                    pixelSize: 5,
                    outlineColor: Cesium.Color.WHITE,
                    outlineWidth: 2,
                    disableDepthTestDistance: Number.POSITIVE_INFINITY
                }
            });
            this.entities.push(entity)
        }
    }
}