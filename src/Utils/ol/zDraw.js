/**
 * 绘制工具
 */
import { Point, LineString, Polygon } from 'ol/geom.js';
import { Vector as VectorLayer } from 'ol/layer.js';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style.js';
import { Vector as VectorSource } from 'ol/source.js';
import Draw from 'ol/interaction/Draw.js';
import Snap from "ol/interaction/Snap.js"
import Overlay from 'ol/Overlay.js';
import { unByKey } from 'ol/Observable.js';
import WKT from 'ol/format/WKT';
/**
 * 当前绘制的要素（Currently drawn feature.）
 * @type {ol.Feature}
 */
let sketch;
/**
 * 帮助提示框对象（The help tooltip element.）
 * @type {Element}
 */
let helpTooltipElement;
/**
 *帮助提示框显示的信息（Overlay to show the help messages.）
 * @type {ol.Overlay}
 */
let helpTooltip;
/**
 *  当用户正在绘制多边形时的提示信息文本
 * @type {string}
 */
let continuePolygonMsg = '继续点击绘制多边形';
/**
 * 当用户正在绘制线时的提示信息文本
 * @type {string}
 */
let continueLineMsg = '继续点击绘制线';
let draw; // global so we can remove it later
let snap = null;
let vector = null;

//全局的地图对象
let Map = null
// 图层属性信息
let Source = null
// 绘制点的
let count = 0
// 是否在统一图层绘制信息
let flage = false
// 地图监听事件
let mapLinstener = null
export default class olDraw {
    constructor(map, source) {
        Map = map
        if (source) {
            Source = source
            flage = true
        }
    }
    /**
     * 开启绘制
     * @param {*} option 
     * type：number类型，1：代表是点，2代表线，3代表多边形
     * style:样式对象，绘制结束后数据显示的样式
     * count: 函数，动态监听绘制的坐标点数
     * end:false ,是否开启自定义结束绘制
     * endDraw：绘制结束后的回调函数，返回绘制要素的坐标信息
     */
    draw(option) {
        count = 0
        let type_ = "LineString"
        if (option.type === 1) {
            type_ = "Point"
        } else if (option.type === 4) {
            type_ = "MultiPoint"
        } else if (option.type === 2) {
            type_ = "LineString"
        } else if (option.type === 3) {
            type_ = "Polygon"
        }
        // 默认样式
        let style = new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.8)'
            }),
            stroke: new Stroke({
                color: '#66b1ff',//#ffcc33
                width: 2
            }),
            image: new CircleStyle({
                radius: 7,
                fill: new Fill({
                    color: '#ffcc33'
                })
            })
        })
        // 创建数据源，如果是同一图层绘制则使用传递进来的数据源，否则每次绘制都创建新的数据源
        let source = null
        if (flage) {
            source = Source
        } else {
            source = new VectorSource()
        }
        let option_ = {
            end: false
        }
        option = { ...option_, ...option }
        // 设置自定义样式
        if (option.style) style = option.style
        //  测量的绘制图层
        vector = new VectorLayer({
            source: source,
            style: style
        });
        // 将图层添加到地图上
        Map.addLayer(vector);

        // 添加绘制事件
        this._addInteraction(source, type_, option.endDraw, option.end);
        //----------- 添加鼠标事件----------------
        //地图容器绑定鼠标移动事件，动态显示帮助提示框内容
        mapLinstener = Map.on('click', function (evt) {
            // 绘制点的
            count += 1
            if (option.count) option.count(count)
        })
        Map.on('pointermove', function (evt) {
            if (evt.dragging) {
                return;
            }
            /** @type {string} */
            var helpMsg = '点击开始绘制';//当前默认提示信息
            //判断绘制几何类型设置相应的帮助提示信息
            if (sketch) {
                var geom = (sketch.getGeometry());
                if (geom instanceof Polygon) {
                    if (count >= 3) {
                        continueLineMsg = "点击继续绘制线,双击结束绘制"
                    }
                    helpMsg = continuePolygonMsg; //绘制多边形时提示相应内容
                } else if (geom instanceof LineString) {
                    if (count >= 2) {
                        continueLineMsg = "点击继续绘制线,双击结束绘制"
                    }
                    helpMsg = continueLineMsg; //绘制线时提示相应内容
                } else if (geom instanceof Point) {

                }
            }

            helpTooltipElement.innerHTML = helpMsg;//将提示信息设置到对话框中显示
            helpTooltip.setPosition(evt.coordinate);//设置帮助提示框的位置
            helpTooltipElement.classList.remove('hidden');//移除帮助提示框的隐藏样式进行显示
        });
        //地图绑定鼠标移出事件，鼠标移出时为帮助提示框设置隐藏样式
        Map.getViewport().addEventListener('mouseout', function () {
            helpTooltipElement.classList.add('hidden');
        });
        return vector
    }
    /**
     * 移除地图绘制图层
     */
    removeDrawlayer() {
        if (vector)
            Map.removeLayer(vector)
    }

    //添加绘制事件
    _addInteraction(source, type = "LineString", callback, end) {
        let _this = this
        draw = new Draw({
            source: source,
            type: type,
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.5)',
                    lineDash: [10, 10],
                    width: 2
                }),
                image: new CircleStyle({
                    radius: 5,
                    stroke: new Stroke({
                        color: 'rgba(255, 255, 255, 0.8)',
                        width: 1
                    }),
                    fill: new Fill({
                        color: '#409EFF'
                    })
                })
            })
        });

        Map.addInteraction(draw);

        //开启捕捉交互功能
        snap = new Snap({ source: source });
        Map.addInteraction(snap);

        this._createHelpTooltip();

        // 绘制开始
        draw.on('drawstart', function (evt) {
            // set sketch
            sketch = evt.feature;
        }, this);
        // 绘制结束
        draw.on('drawend', () => {
            var format = new WKT();
            if (callback) callback({
                feature:sketch,
                coordinate:sketch.getGeometry().getCoordinates(),
                wkt:format.writeFeature(sketch)
            })
            if (!end)
                this._removeDraw()
        }, this);
    }

    /**
    * 创建一个新的帮助提示框
    */
    _createHelpTooltip() {
        if (helpTooltipElement) {
            helpTooltipElement.parentNode.removeChild(helpTooltipElement);
        }
        helpTooltipElement = document.createElement('div');
        helpTooltipElement.className = 'tooltip hidden';
        helpTooltip = new Overlay({
            element: helpTooltipElement,
            offset: [15, 0],
            positioning: 'center-left'
        });
        Map.addOverlay(helpTooltip);
    }
    /**
     * 移除绘制函数
     * @param {*} callback 
     */
    _removeDraw() {
        // unset sketch
        sketch = null;
        // 结束量测绘制
        Map.removeInteraction(draw);
        // 移除焦点捕捉
        Map.removeInteraction(snap);
        // 隐藏提示框
        Map.removeOverlay(helpTooltip);
        // 移除地图点击事件
        unByKey(mapLinstener)
    }
    /**
     * 结束绘制
     */
    stopDraw() {
        if (draw){
            draw.finishDrawing()
            this._removeDraw()
        }
            
    }

}