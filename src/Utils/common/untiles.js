import md5 from 'js-md5'
import { Message } from 'element-ui';
let Utiles = {
    /**
     * md5数据加密
     * @param {*} str 
     */
    md5(str) {
        let pwd = str.toUpperCase();
        pwd = md5(str);
        return pwd;
    },
    /**
     * 信息提示工具
     * @param {*} msg     提示信息
     * @param {*} type    1:代表成功，2:代表警告，3：代表信息，4：代表错误
     */
    msg(msg = "提示", type = 1) {
        let type_ = 'success'
        switch (type) {
            case 1:
                type_ = 'success'
                break
            case 2:
                type_ = 'warning'
                break
            case 3:
                type_ = 'info'
                break
            case 4:
                type_ = 'error'
                break
        }
        Message({
            title: '失败',
            message: msg,
            type: type_,
            offset: 60
        })
    }
}

export default Utiles