import request from "../Utils/common/request"
export function getAccesses(id) {
    let url = "/access/accessList"
    return request.getRequest(url, {
        
    },{
        openPage:false
    })
}
/**
 * 添加角色
 * @param {*} params 
 */
export function addAccess(params = {}) {
    let url = "/access"
    return request.postRequet(url, params)
}

/**
 * 修改角色
 * @param {*} params 
 */
export function updateAccess(params = {}) {
    let url = "/access/update"
    return request.postRequet(url, params)
}
/**
 * 根据id删除权限
 * @param {ge} id 
 */
export function deletAccess(id) {
    let url = "/access/delete"
    return request.postRequet(url, {
        id
    })
}
/**
 * 获取权限信息
 * @param {*} params 
 */
export function addRoleAccess(params={}){
    let url = "/roleaccess"
    return request.postRequet(url, params)
}

export function getAccessesByRole(role_id){
    let url = "/roleaccess/role"
    return request.postRequet(url, {
        role_id
    })
}