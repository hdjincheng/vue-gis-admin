import request from "../Utils/common/request"
export function getRoles(id) {
    let url = "/role/roleList"
    return request.getRequest(url, {})
}
/**
 * 添加角色
 * @param {*} params 
 */
export function addRole(params = {}) {
    let url = "/role"
    return request.postRequet(url, params)
}

/**
 * 修改角色
 * @param {*} params 
 */
export function updateRole(params = {}) {
    let url = "/uRole"
    return request.postRequet(url, params)
}

export function deletRole(id) {
    let url = "/dRole"
    return request.postRequet(url, {
        id
    })
}