import request from "../Utils/common/request"
/**
 * 获取多边形的geojson格式数据
 * @param {*} id 
 */
export function getPolygonData() {
    let url = "/polygon"
    return request.getRequest(url,{})
}
/**
 * 将多边形插入数据库中
 * @param {*} params 
 */
export function addPolygonFeature(params={}){
    let url = "/polygon/add"
    return request.postRequet(url, params)
}
/**
 * 删除多边形要素
 * @param {*} params 
 */
export function deletPolygonFeature(params={}){
    let url = "/polygon/delete"
    return request.postRequet(url, params)
}
/**
 * 修改多边形
 * @param {*} params 
 */
export function updatePolygonFeature(params={}){
    let url = "/polygon/update"
    return request.postRequet(url, params)
}