
import request from "../Utils/common/request"
export function getUesrs(id) {
    let url = "/user/userList"
    return request.getRequest(url, {
        id: id ? id : ""
    })
}
/**
 *  添加用户信息
 * @param {*} data 
 */
export function addUser(data = {}) {
    let url = "/user"
    return request.postRequet(url, data)
}
/**
 *用户更新接口
 * @param {s} data 
 */
export function updateUser(data = {}) {
    let url = "/user/update"
    return request.postRequet(url, data)

}
/**
 * 根据id移除用户信息
 * @param {*} id 
 */
export function deleteUser(id = null) {
    let url = "/user/delete"
    return request.postRequet(url, { id: id })
}