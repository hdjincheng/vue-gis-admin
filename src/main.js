import Vue from 'vue'
import App from './App.vue'

// 引入路由
import router from './router/index'
// 引入vuex
import store from './store/index'
// 引入icon图标组件
import iconPicker from 'e-icon-picker';
import 'e-icon-picker/dist/index.css';//基础样式
import 'e-icon-picker/dist/main.css'; //fontAwesome 图标库样式
Vue.use(iconPicker);//使用e-icon-picker

// 引入elementuiui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
// 引入全局css文件
import "@/assets/css/base.css"
import "@/assets/css/map_ol.css"
import "@/assets/css/common-el.css";
import "@/assets/css/c3d.css";

Vue.config.productionTip = false

import 'cesium/Widgets/widgets.css'

new Vue({
  router,
  store,
  render: h => h(App),

}).$mount('#app')
